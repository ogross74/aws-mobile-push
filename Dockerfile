FROM anapsix/alpine-java
COPY target/mobile-push-0.0.1-SNAPSHOT.jar /opt/spring-cloud/lib/
ENTRYPOINT ["/opt/jdk/bin/java"]
CMD ["-jar", "/opt/spring-cloud/lib/mobile-push-0.0.1-SNAPSHOT.jar"]
EXPOSE 8898
