package com.ogross74.mobilepush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilePushApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobilePushApplication.class, args);
	}
}
