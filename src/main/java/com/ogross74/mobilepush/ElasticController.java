package com.ogross74.mobilepush;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/elastic")
public class ElasticController {

    /*
    elasticsearch.host = localhost
    elasticsearch.port = 9200
     */

    @Value("${elasticsearch.host}")
    String host;

    @Value("${elasticsearch.rest.port}")
    String port;



    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String status() {
        String uri = "http://"+host+":"+port;
        return restTemplate.getForObject(uri, String.class);
    }

    @RequestMapping(value = "/indexes", method = RequestMethod.GET)
    public String indexes() {
        //http://localhost:9200/_cat/indices?h=index,store.size&bytes=k&format=json
        String uri = "http://"+host+":"+port+"/_cat/indices?h=index,store.size&bytes=k&format=json";
        return restTemplate.getForObject(uri, String.class);
    }


}
