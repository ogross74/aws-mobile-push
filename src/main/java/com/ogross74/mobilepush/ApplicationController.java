package com.ogross74.mobilepush;

import com.ogross74.mobilepush.application.Application;
import com.ogross74.mobilepush.application.ApplicationBuilder;
import com.ogross74.mobilepush.application.ApplicationService;
import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/application")
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;

    @Autowired
    ElasticsearchTemplate esTemplate;

    /*
    Currently there's no different between POST and PUT meaning add and update. Should there be one?
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Application> add(@RequestBody Application application) {
        Application response = applicationService.save(application);
        return new ResponseEntity<Application>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public ResponseEntity<Application> update(@RequestBody Application application) {
        Application response = applicationService.save(application);
        return new ResponseEntity<Application>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Application>> id(@PathVariable("id") String id) {
        Optional<Application> response = applicationService.findById(id);
        return new ResponseEntity<Optional<Application>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") String id) {
        applicationService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public ResponseEntity<Application> name(@PathVariable("name") String name) {
        Application response = applicationService.findByName(name);
        return new ResponseEntity<Application>(response, HttpStatus.OK);
    }



    @RequestMapping(value = "/demo", method = RequestMethod.POST)
    public ResponseEntity<Application> demo() {
        Application application = ApplicationBuilder.builder().setAndroidSnsArn("fakeAndroidSns").setIosSnsArn("fakeIosSns").setId(UUID.randomUUID().toString()).setName("fake name").build();
        Application response = applicationService.save(application);
        return new ResponseEntity<Application>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<Application>> list() {
        Iterable<Application> all = applicationService.findAll();
        return new ResponseEntity<List<Application>>(IteratorUtils.toList(all.iterator()), HttpStatus.OK);
    }

}
