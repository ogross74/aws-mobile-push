package com.ogross74.mobilepush.application;

import java.util.Optional;

public interface ApplicationService {

    Application save(Application application);

    void delete(Application application);

    void deleteById(String id);

    Optional<Application> findById(String id);

    Application findByName(String name);

    Iterable<Application> findAll();


}
