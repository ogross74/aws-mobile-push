package com.ogross74.mobilepush.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private ApplicationRepository repository;

    @Autowired
    public void setRepository(ApplicationRepository repository) {
        this.repository = repository;
    }

    @Override
    public Application save(Application application) {
        return repository.save(application);
    }

    @Override
    public void delete(Application application) {
        repository.delete(application);
    }

    @Override
    public void deleteById(String id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Application> findById(String id) {
        return repository.findById(id);
    }

    @Override
    public Application findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public Iterable<Application> findAll() {
        return repository.findAll();
    }
}
