package com.ogross74.mobilepush.application;

import com.ogross74.mobilepush.BuilderProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "config", type = "application")
public class Application {


    @Id
    private String id;

    private String name,androidSnsArn,iosSnsArn;


    public String getName() {
        return name;
    }

    @BuilderProperty
    public void setName(String name) {
        this.name = name;
    }

    public String getAndroidSnsArn() {
        return androidSnsArn;
    }
    @BuilderProperty
    public void setAndroidSnsArn(String androidSnsArn) {
        this.androidSnsArn = androidSnsArn;
    }

    public String getIosSnsArn() {
        return iosSnsArn;
    }

    Application(String id, String name, String androidSnsArn, String iosSnsArn) {
        this.id = id;
        this.name = name;
        this.androidSnsArn = androidSnsArn;
        this.iosSnsArn = iosSnsArn;
    }

    @BuilderProperty
    public void setIosSnsArn(String iosSnsArn) {
        this.iosSnsArn = iosSnsArn;
    }

    public String getId() {
        return id;
    }

    public Application() {
    }
}
