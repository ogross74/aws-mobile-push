package com.ogross74.mobilepush.application;

public class ApplicationBuilder {
    private String id;
    private String name;
    private String androidSnsArn;
    private String iosSnsArn;

    public ApplicationBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public ApplicationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ApplicationBuilder setAndroidSnsArn(String androidSnsArn) {
        this.androidSnsArn = androidSnsArn;
        return this;
    }

    public ApplicationBuilder setIosSnsArn(String iosSnsArn) {
        this.iosSnsArn = iosSnsArn;
        return this;
    }

    private ApplicationBuilder() {}

    public static ApplicationBuilder builder() {
        return new ApplicationBuilder();
    }

    public Application build() {
        return new Application(id, name, androidSnsArn, iosSnsArn);
    }
}