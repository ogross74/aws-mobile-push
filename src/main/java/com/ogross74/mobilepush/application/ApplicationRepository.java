package com.ogross74.mobilepush.application;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface ApplicationRepository extends ElasticsearchRepository<Application, String> {

    Application findByName(String name);

    /*
    @Query("{\"bool\": {\"must\": [{\"match\": {\"authors.name\": \"?0\"}}]}}")
    Page<Application> findByNameUsingCustomQuery(String name, Pageable pageable);
    */
}
